import java.util.*;

public class EvenOddMixed {
    public static int find(int[] arr) {
        int n = arr.length, odd = 0, even = 0;
        for (int i = 0; i < n; i++) {
            if (arr[i] % 2 == 0) {
                even++;
            } else {
                odd++;
            }
        }

        if (even == n) {
            return 1;
        } else if (odd == n) {
            return 2;
        } else {
            return 3;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n;
        System.out.println("Enter the number of elements");
        n = sc.nextInt();

        int[] arr = new int[n];
        System.out.println("Enter the array elements");
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }

        sc.close();

        int ans = find(arr);

        if (ans == 1)
            System.out.println("Even");
        else if (ans == 2)
            System.out.println("Odd");
        else
            System.out.println("Mixed");
    }
}